﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wiskunde.Meetkunde
{
    class Vormen
    {
        private ConsoleColor kleur;
        public ConsoleColor Kleur
        {
            get { return kleur; }
            set
            {
                kleur = value; // value is impliciete parameter van setter
            }
        }

        public static string Lijn(int lengte)
        {
            return new string('-', lengte);
        }

        public static string Lijn(int lengte, char teken)
        {
            return new string(teken, lengte);
        }
    }
}
