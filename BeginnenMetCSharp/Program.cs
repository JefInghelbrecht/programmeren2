﻿using System;
using Wiskunde.Meetkunde;

namespace BeginnenMetCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            string familienaam = "Jan Janssens";
            Console.WriteLine("Hello World!");
            Console.WriteLine(familienaam);
            // Vormen vormen = new Vormen();
            Console.WriteLine(Vormen.Lijn(30));
            Console.WriteLine(Vormen.Lijn(50, '*'));
            Console.ReadKey();
        }
    }
}
