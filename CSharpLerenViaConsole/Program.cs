﻿using System;
using System.Collections.Generic;
using Blog;

namespace CSharpLerenViaConsole
{
    class Program
    {
        static void GeefPersoongegevensIn()
        {
            string voornaam;
            string familienaam;
            string straat;
            string postcode;
            string stad;
            List<Persoon> persoonLijst = new List<Persoon>();
            do
            {
                Console.Clear();
                Console.Write("Typ een voornaam in: ");
                voornaam = Console.ReadLine();
                if (voornaam != "Q")
                {
                    Console.Write("Typ een familienaam in: ");
                    familienaam = Console.ReadLine();
                    if (familienaam != "Q")
                    {
                        Console.Write("Typ een straat in: ");
                        straat = Console.ReadLine();
                        if (straat != "Q")
                        {
                            Console.Write("Typ een postcode in: ");
                            postcode = Console.ReadLine();
                            if (postcode != "Q")
                            {
                                Console.Write("Typ een stad in: ");
                                stad = Console.ReadLine();
                                // maakt een instantie/exemplaar van de klasse persoon
                                Persoon persoonsInstantie = new Persoon();
                                // We gebruiken de Voornaam property van de instantie
                                // persoonsInstantie om de waarde van voornaam toe te kennen
                                // aan het verld voornaam van de persoonsInstantie.
                                persoonsInstantie.Voornaam = voornaam;
                                persoonsInstantie.Familienaam = familienaam;
                                persoonsInstantie.Straat = straat;
                                persoonsInstantie.Postcode = postcode;
                                persoonsInstantie.Stad = stad;
                                // we voegen de nieuw gemaakte instantie toe
                                // aan de lijst met persoonsinstanties
                                persoonLijst.Add(persoonsInstantie);
                                Console.WriteLine("Je hebt de volgende persoonsgegevens ingetypt:");
                                Console.WriteLine($"Voornaam: {persoonsInstantie.Voornaam}");
                                Console.WriteLine($"Familienaam: {persoonsInstantie.Familienaam}");
                                Console.ReadKey();
                            }
                        }
                    }
                }
            }
            while (voornaam != "Q");
            // schrijf de lijst met persoonsinstanties naar de harde schijf
            Persoon persoonInstantie = new Persoon();
            persoonInstantie.SerializeObjectToCsv(persoonLijst, ";");


        }

        static void GeefPostcodesIn()
        {
            string postcode;
            string stad;
            string provincie;
            string localite;
            string province;
            // declareer een generieke lijst waarin we
            // instanties of objecten van postcodes zullen plaatsen
            List<Postcode> postcodeList = new List<Postcode>();

            do
            {
                Console.Clear();
                Console.Write("Typ een postcode in: ");
                postcode = Console.ReadLine();
                if (postcode == "Q")
                {
                    break;
                }
                Console.Write("Typ de naam van de stad in: ");
                stad = Console.ReadLine();
                if (stad == "Q")
                {
                    break;
                }
                Console.Write("Typ de provincie van de province in: ");
                provincie = Console.ReadLine();
                if (provincie == "Q")
                {
                    break;
                }
                Console.Write("Typ de naam in het frans van de stad in: ");
                localite = Console.ReadLine();
                if (localite == "Q")
                {
                    break;
                }
                Console.Write("Typ de naam in het frans van de provindie in: ");
                province = Console.ReadLine();
                // ik wil de ingetypte gegevens van de postcode opslaan
                // Waarin? Nu staan ze in string variabelen
                // telkens als er een nieuwe postcode wordt ingetypt
                // worden dezelfde string variabelen gebruikt. Dus de vorige
                // postcode wordt overschreven
                // Wat komt ons redden?
                // de Postocode klasse. De klasse bevat een beschrijving van
                // de postcode, dwz maakt een veld en properties voor elk kenmerk
                // van de postocode: Plaats, Stad, Provincie, Localite, Ville en een Province
                // Ik moet nu eerst een object, een instantie maken van de klasse:
                Postcode objectPostcode = new Postcode();
                // ik wil de waarden die in de string variabelen staan in de
                // velden van de objectPostcode instantie kopiëren.
                // Daarvoor gebruik ik de setters van de properties:
                objectPostcode.Plaats = stad;
                objectPostcode.Provincie = provincie;
                objectPostcode.Localite = localite;
                objectPostcode.Province = province;
                objectPostcode.Code = postcode;
                // Voor elke postcode maak ik een nieuw object, dwz dat
                // de gegevens van de vorige postcode niet worden overschreven
                // Elk ingegeven postcode heeft zijn eigen object
                //
                // de instantie objectPostcode aan generieke lijst toevoegen
                postcodeList.Add(objectPostcode);
            } while (postcode != "Q");
            Console.WriteLine("De lijst van de ingevoerde postcodes:");
            foreach (Postcode postcodeItem in postcodeList)
            {
                Console.WriteLine("{0} {1} {2} {3} {4}, ",
                    postcodeItem.Code, postcodeItem.Plaats, postcodeItem.Provincie,
                    postcodeItem.Localite, postcodeItem.Province);
            }
            // beschik over een instantie van Postcode klasse?
            // Nee, ik moet dus eerst een instantie van de Postcode klasse maken
            Postcode mijnPostcode = new Postcode();
            string message = mijnPostcode.SerializeObjectToCsv(postcodeList, "$");
            Console.WriteLine(message);
        }

        static void Main(string[] args)
        {
            // Console.WriteLine("Hello Crescendo World!");
            // CSharpLerenViaConsole.WerkenMetGegevens.CharLerenGebruiken();
            // ShowAscii is niet static
            // eerst instantie maken
            // WerkenMetGegevens werkenMetGegevens = new WerkenMetGegevens();
            // werkenMetGegevens.ShowAllAsciiValues();
            // Console.ReadKey();
            // WerkenMetGegevens.showCultureInfo();
            // WerkenMetGegevens.stringConcatenationVersuStringBuilder();
            // Console.ReadKey();
            // WerkenMetGegevens werkenmetGegevens = new WerkenMetGegevens();
            // werkenmetGegevens.WerkenMetStruct();
            // WerkenMetGegevens.TryOutListArray();
            //Persoon afzender = new Persoon();
            //afzender.voornaam = "Mohamed";
            //afzender.familienaam = "El Farisi";
            //afzender.Leeftijd = -22;
            //Persoon ontvanger = new Persoon();
            //ontvanger.voornaam = "Mathilde";
            //ontvanger.familienaam = "De Coninck";
            //ontvanger.Leeftijd = 24;
            //Persoon ontvanger2 = new Persoon();
            //ontvanger2.voornaam = "Jan";
            //ontvanger2.familienaam = "Dewilde";
            //ontvanger2.Leeftijd = 233;
            //string afzendertekst = afzender.ShowInfo();
            //Console.WriteLine(afzendertekst);
            //string ontvangertekst = ontvanger.ShowInfo();
            //Console.WriteLine(ontvangertekst);
            //string ontvanger2tekst = ontvanger2.ShowInfo();
            //Console.WriteLine(ontvanger2tekst);
            //Helpers.Tekstbestand mijnTekstbestand = new Helpers.Tekstbestand();
            //// de setter van de property om een waarde aan het private
            //// field toe te kennen
            //mijnTekstbestand.FileName = "Data\\Postcode.csv";
            //Console.WriteLine("Melding: {0}", mijnTekstbestand.Melding);

            //// de getter van de property om de waaarde uit het private field
            //// op te halen
            //Console.WriteLine("Het bestand heet {0}", mijnTekstbestand.FileName);
            //// Good practice: manipulation data inside class (encapsulation)
            //Console.WriteLine("De volledige bestandsnaam is {0}", mijnTekstbestand.FullName);
            //// Bad practice: gegevensmanipulatie buiten de klasse in de calling program
            //Console.WriteLine("De volledige bestandsnaam is c:\\data\\{0}", mijnTekstbestand.FileName);
            //// Tekst inlezen
            //if (mijnTekstbestand.Lees())
            //{
            //    // Toon de ingelezen tekst
            //    Console.WriteLine($"De tekst in het bestand is \n{mijnTekstbestand.Text}");
            //}
            //else
            //{
            //    Console.WriteLine($"Het bestand heet {mijnTekstbestand.Melding}");
            //}
            //Console.WriteLine("Typ een postcode in: ");
            //Console.ReadLine();
            // Zoek de postcode
            //Postcode postcode = new Postcode();
            //Console.WriteLine(postcode.ReadPostcodesFromCSVFile());
            // postcode.GetPostcodeList();
            // GeefPostcodesIn();
            //GeefPersoongegevensIn();
            Persoon enePersoon = new Persoon();
            string[] persoonArray = enePersoon.GetArray();
            List<Persoon> persoonsLijst = enePersoon.GetList(persoonArray);
            Console.WriteLine("Klik op een toets om het programma te beëindigen");
            Console.ReadKey();
        }
    }
}
